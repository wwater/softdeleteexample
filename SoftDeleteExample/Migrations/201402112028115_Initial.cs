namespace SoftDeleteExample.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Customer",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Product",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        CustomerId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Customer", t => t.CustomerId, cascadeDelete: true)
                .Index(t => t.CustomerId);
            
            CreateTable(
                "dbo.Person",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Customer_Id = c.Int(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Customer", t => t.Customer_Id)
                .Index(t => t.Customer_Id);
            
            CreateTable(
                "dbo.PersonProduct",
                c => new
                    {
                        Person_Id = c.Int(nullable: false),
                        Product_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Person_Id, t.Product_Id })
                .ForeignKey("dbo.Person", t => t.Person_Id, cascadeDelete: true)
                .ForeignKey("dbo.Product", t => t.Product_Id, cascadeDelete: true)
                .Index(t => t.Person_Id)
                .Index(t => t.Product_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Person", "Customer_Id", "dbo.Customer");
            DropForeignKey("dbo.PersonProduct", "Product_Id", "dbo.Product");
            DropForeignKey("dbo.PersonProduct", "Person_Id", "dbo.Person");
            DropForeignKey("dbo.Product", "CustomerId", "dbo.Customer");
            DropIndex("dbo.Person", new[] { "Customer_Id" });
            DropIndex("dbo.PersonProduct", new[] { "Product_Id" });
            DropIndex("dbo.PersonProduct", new[] { "Person_Id" });
            DropIndex("dbo.Product", new[] { "CustomerId" });
            DropTable("dbo.PersonProduct");
            DropTable("dbo.Person");
            DropTable("dbo.Product");
            DropTable("dbo.Customer");
        }
    }
}
