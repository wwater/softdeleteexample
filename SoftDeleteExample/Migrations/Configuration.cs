namespace SoftDeleteExample.Migrations
{
    using SoftDeleteExample.Entities;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<SoftDeleteExample.DataContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(SoftDeleteExample.DataContext context)
        {
            foreach (var customer in context.Customers.ToList())
            {
                context.Customers.Remove(customer);
            }
            context.SaveChanges();

            var customer1 = new Entities.Customer() { Name = "customer 1" };

            var product1 = new Product() { Name = "product 1" };
            var product2 = new Product() { Name = "product 2" };
            customer1.Products.Add(product1);
            customer1.Products.Add(product2);

            var user1 = new Person() { Name = "John Doe" };
            var user2 = new Person() { Name = "Jane Doe" };
            product1.Persons.Add(user1);
            product1.Persons.Add(user2);
            product2.Persons.Add(user1);
            product2.Persons.Add(user2);

            context.Customers.Add(customer1);

            context.SaveChanges();            
        }
    }
}
