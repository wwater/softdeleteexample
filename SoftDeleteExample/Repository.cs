﻿using SoftDeleteExample.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftDeleteExample
{
    public class Repository
    {
        private DataContext context;
        public Repository()
        {
            this.context = new DataContext();
        }

        public IEnumerable<Person> GetUsers()
        {
            return context.Persons.ToList();
        }
    }
}
