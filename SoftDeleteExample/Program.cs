﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftDeleteExample
{
    /// <summary>
    /// Soft delete example, based on:
    /// http://www.wiktorzychla.com/2013/10/soft-delete-pattern-for-entity.html
    /// </summary>
    public class Program
    {
        static void Main(string[] args)
        {
            var context = new DataContext();

            WriteUser(context);

            Console.WriteLine("Soft deleted first product");
            var firstProduct = context.Products.First();
            context.Products.Remove(firstProduct);
            context.SaveChanges();
            WriteUser(context);

            Console.WriteLine("Soft deleted first person of first product");
            var user = context.Products.First().Persons.First();
            context.Persons.Remove(user);
            context.SaveChanges();
            WriteUser(context);

            Console.ReadLine();
        }

        private static void WriteUser(DataContext context)
        {
            foreach (var customer in context.Customers)
            {
                Console.WriteLine(customer.Name);

                foreach (var products in customer.Products)
                {
                    Console.WriteLine("- " + products.Name);

                    foreach (var user in products.Persons)
                    {
                        Console.WriteLine("-- " + user.Name);
                    }
                }
            }
        }
    }
}
