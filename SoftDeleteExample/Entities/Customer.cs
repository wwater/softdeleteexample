﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftDeleteExample.Entities
{
    public class Customer : BaseEntity
    {
        public Customer()
        {
            this.Users = new HashSet<Person>();
            this.Products = new HashSet<Product>();
        }
        public string Name { get; set; }
        public virtual ICollection<Person> Users { get; set; }
        public virtual ICollection<Product> Products { get; set; }
    }
}
