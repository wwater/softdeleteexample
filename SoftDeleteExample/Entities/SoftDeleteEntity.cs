﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftDeleteExample.Entities
{
    public class SoftDeleteEntity: BaseEntity
    {
        [NotMapped]
        public bool IsDeleted { get; set; }
    }
}
