﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftDeleteExample.Entities
{
    public class Person: SoftDeleteEntity
    {
        public Person()
        {
            this.OrderedProducts = new HashSet<Product>();
        }
        public string Name { get; set; }
        public virtual ICollection<Product> OrderedProducts { get; set; }
    }
}
