﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftDeleteExample.Entities
{
    public class Product : SoftDeleteEntity
    {
        public Product()
        {
            this.Persons = new HashSet<Person>();
        }
        public string Name { get; set; }
        public int CustomerId { get; set; }
        public virtual Customer Customer { get; set; }
        public virtual ICollection<Person> Persons { get; set; }
    }
}
